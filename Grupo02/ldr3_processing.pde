import processing.serial.*; //importa la libreria de comunicación serial
Serial port;

PrintWriter output;
int valor;
float aguja;
float angulo;
PImage barra;
PImage pointer;

//colores del circulo
float rojo;
float verde;
float azul;

void setup(){
println(Serial.list()); //visualizar los puertos seriales disponibles
port =new Serial (this,"COM4",9600); //abre el puerto serial 
output=createWriter("Datos_de_luz.txt"); // crea el archivo de texto
size(800,600); // crea la ventana del tamaño definido
barra=loadImage("fondo.jpg");  //carga las imagenes
pointer=loadImage("pointer.png"); 
}

void draw() {
background(barra); // fondo de la imagen seleccionada

if (valor<51){  //la barra tiene un máximo de 50 lumen
aguja = 3.6*valor;  // queda un angulo en grados para el valor obtenido desde el Arduino
angulo=(aguja-90)*(PI/180);} //transforma el angulo a radianes. Se resta 90 para que 0 este a la izquierda 
else if (valor>51){
angulo=PI*0.5;} //para que la aguja apunte al máximo

//visualizar texto
text("Nivel de luz =",290,500);
text(valor,380,500);
text("lumen",400,500);
//Escribir los datos del nivel de luz con el tiempo (h/m/s) en el archivo de texto
output.print(valor + "Lumen");
output.print(hour()+":");
output.print(minute()+":");
output.println(second());
output.println("");
   
float nluz = map(valor, 0,50, 0, 255);//Se escala el nivel de luz donde el max. es 50 y mínimo 0
rojo=nluz;
verde=nluz*-1 + 255;
azul=nluz*-1 + 255;
  //Se dibuja un circulo para colorear segun el nivel de luz
noStroke();
fill(rojo,verde,azul); //para colorear el circulo y las letras
ellipseMode(CENTER); //la forma del circulo
ellipse(490,500,50,50); // posicion y tamaño del circulo

translate(width/2, height/2); // el origen se traslada al centro de la interfase
rotate(angulo); //para que se mueva la aguja
image(pointer,-pointer.width/2,-pointer.height/2+7); //se selecciona la imagen de la aguja y se define su posición
}

void serialEvent(Serial p){
  String dato = p.readStringUntil('\n'); // lee el buffer del puerto serie
  
  if(dato != null){
    dato = trim(dato); 
    valor = int(dato); 
  }
}
